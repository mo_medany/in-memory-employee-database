package com.mondiamedia.memory;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.Assertion;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemOutRule;

/**
 * @author medany
 */

public class DatabaseTest {

	@Rule
	public final ExpectedSystemExit exit = ExpectedSystemExit.none();
	@Rule
	public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

	@Test
	public void addEmployee() {

		database.runCommandWithParams("empty");

		commands = new String[] { "add", "add a001", "add 1001", "add 1001-Ali12", "add 1001-Ali-Java Programmer",
				"add 1001-Ali-Java Programmer21", "add 1001-Ali-Java Programmer-aa10000",
				"add 1001-Ali-Java Programmer-10000" };

		actuals = new String[commands.length];
		expecteds = new String[] { "add <employee id>-<employee name>-<designation>-<monthly salary>",
				"add <employee id>-<employee name>-<designation>-<monthly salary>",
				"add <employee id>-<employee name>-<designation>-<monthly salary>",
				"add <employee id>-<employee name>-<designation>-<monthly salary>",
				"add <employee id>-<employee name>-<designation>-<monthly salary>",
				"add <employee id>-<employee name>-<designation>-<monthly salary>",
				"add <employee id>-<employee name>-<designation>-<monthly salary>",
				"Employee 'Ali' added successfully. Total no of employees = 1" };

		for (int i = 0; i < commands.length; i++) {
			actuals[i] = database.runCommandWithParams(commands[i]);
		}

		Assert.assertArrayEquals(expecteds, actuals);

	}

	@Test
	public void deleteEmployee() {

		database.runCommandWithParams("empty");

		commands = new String[] { "del", "del a1002", "del 1002 aa", "del 1002" };

		actuals = new String[commands.length];
		expecteds = new String[] { "del <employee id>", "del <employee id>", "del <employee id>",
				"Employee '1002' not found" };

		for (int i = 0; i < commands.length; i++) {
			actuals[i] = database.runCommandWithParams(commands[i]);
		}

		Assert.assertArrayEquals(expecteds, actuals);

	}

	@Test
	public void updateEmployee() {

		database.runCommandWithParams("empty");

		commands = new String[] { "update", "update 1002", "update 1002aa", "update 1002-SALAR", "update 1002-SAL-ss",
				"update 1002a-SALARY-25000", "add 1002-Ahmed-IT Manager-22000", "update 1002-NAME-25000",
				"update 1002-DESIG-25000", "update 1002-SALARY-25000" };

		actuals = new String[commands.length];
		expecteds = new String[] { "update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"Employee 'Ahmed' added successfully. Total no of employees = 1",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"update <employee id>-<NAME/DESIG/SALARY>-<New Value>",
				"Employee '1002' updated. Name: Ahmed, Designation: IT Manager, Salary: 25000" };

		for (int i = 0; i < commands.length; i++) {
			actuals[i] = database.runCommandWithParams(commands[i]);
		}

		Assert.assertArrayEquals(expecteds, actuals);

	}

	@Test
	public void printEmployee() {

		database.runCommandWithParams("empty");

		commands = new String[] { "print", "print 1001a", "print 1001 aa", "add 1001‐Ali‐Java Programmer‐10000",
				"print 1001" };

		actuals = new String[commands.length];
		expecteds = new String[] { "print <employee id>", "print <employee id>", "print <employee id>",
				"Employee 'Ali' added successfully. Total no of employees = 1",
				"Ali, Designation Java Programmer, Salary: 10000" };

		for (int i = 0; i < commands.length; i++) {
			actuals[i] = database.runCommandWithParams(commands[i]);
		}

		Assert.assertArrayEquals(expecteds, actuals);

	}

	@Test
	public void printAllEmployees() {

		database.runCommandWithParams("empty");

		commands = new String[] { "add 1001‐Ali‐Java Programmer‐10000", "add 1003‐Fouad‐HR Officer‐20000",
				"add 1002‐Ahmed‐IT Manager‐22000", "printall", "printall aaa", "printall 123", "printall ASC1",
				"printall ASC", "printall DESC" };

		actuals = new String[commands.length];
		expecteds = new String[] { "Employee 'Ali' added successfully. Total no of employees = 1",
				"Employee 'Fouad' added successfully. Total no of employees = 2",
				"Employee 'Ahmed' added successfully. Total no of employees = 3", "printall <ASC/DESC>",
				"printall <ASC/DESC>", "printall <ASC/DESC>", "printall <ASC/DESC>",
				"Ahmed : IT Manager\nAli : Java Programmer\nFouad : HR Officer",
				"Fouad : HR Officer\nAli : Java Programmer\nAhmed : IT Manager" };

		for (int i = 0; i < commands.length; i++) {
			actuals[i] = database.runCommandWithParams(commands[i]);
		}

		Assert.assertArrayEquals(expecteds, actuals);

	}

	@Test
	public void quit() {
		exit.expectSystemExitWithStatus(0);

		exit.checkAssertionAfterwards(new Assertion() {
			@Override
			public void checkAssertion() throws Exception {
				actual = systemOutRule.getLog();
				Assert.assertEquals(expected, actual);
			}
		});

		expected = "The application will exit.";

		database.runCommandWithParams("Quit");
	}

	@Test
	public void bulkCommands() {

		database.runCommandWithParams("empty");

		exit.expectSystemExitWithStatus(0);
		exit.checkAssertionAfterwards(new Assertion() {
			@Override
			public void checkAssertion() throws Exception {
				actuals[actuals.length - 1] = systemOutRule.getLog();
				Assert.assertArrayEquals(expecteds, actuals);
			}
		});

		commands = new String[] { "add 1001-Ali-Java Programmer-10000", "del 1002", "add 1002-Ahmed-IT Manager-22000",
				"add 1003-Fouad-HR Officer-20000", "update 1002-SALARY-25000", "print 1001", "printall ASC" };

		actuals = new String[commands.length + 1];
		expecteds = new String[] { "Employee 'Ali' added successfully. Total no of employees = 1",
				"Employee '1002' not found", "Employee 'Ahmed' added successfully. Total no of employees = 2",
				"Employee 'Fouad' added successfully. Total no of employees = 3",
				"Employee '1002' updated. Name: Ahmed, Designation: IT Manager, Salary: 25000",
				"Ali, Designation Java Programmer, Salary: 10000",
				"Ahmed : IT Manager\nAli : Java Programmer\nFouad : HR Officer", "The application will exit." };

		for (int i = 0; i < commands.length; i++) {
			actuals[i] = database.runCommandWithParams(commands[i]);
		}

		database.runCommandWithParams("Quit");
	}

	private final DataBase database = new DataBase();
	private String actual, expected;
	private String[] actuals, expecteds, commands;

}
