package com.mondiamedia.memory.db;

/**
 * @author medany
 */

public class AppConstants {

	/**
	 * COMMANDS
	 */
	public static final String COMMAND_ADD = "add";
	public static final String COMMAND_DELETE = "del";
	public static final String COMMAND_UPDATE = "update";
	public static final String COMMAND_PRINT = "print";
	public static final String COMMAND_PRINT_ALL = "printall";
	public static final String COMMAND_QUIT = "quit";
	public static final String COMMAND_EMPTY = "empty";

	/**
	 * SORT ORDER
	 */
	public static final String SORT_ASCENDING = "ASC";
	public static final String SORT_DESCENDING = "DESC";

	/**
	 * UPDATE ACTIONS
	 */
	public static final String UPDATE_ACTION_NAME = "NAME";
	public static final String UPDATE_ACTION_DESIG = "DESIG";
	public static final String UPDATE_ACTION_SALARY = "SALARY";

}
