package com.mondiamedia.memory.db;

import com.mondiamedia.memory.db.exceptions.TypeAlreadyFoundException;
import com.mondiamedia.memory.db.exceptions.TypeNotFoundException;

/**
 * {@code DDLActions} represents basic {@code CREATE}, {@code DROP}, and
 * {@code TRUNCATE} DDL commands.
 * 
 * @author medany
 */

public interface DDLActions {

	/**
	 * Creates specified type in this store. {@code TypeAlreadyFoundException} is
	 * thrown when specified type is present.
	 * 
	 * @param clazz
	 *            type to create
	 * @throws TypeAlreadyFoundException
	 *             if specified type is present
	 */
	public void create(String clazz) throws TypeAlreadyFoundException;

	/**
	 * Drops specified type from this store. {@code TypeNotFoundException} is thrown
	 * when specified type not present.
	 * 
	 * @param clazz
	 *            type to drop
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 */
	public void drop(String clazz) throws TypeNotFoundException;

	/**
	 * Empty specified type in this store. {@code TypeNotFoundException} is thrown
	 * when specified type is not found.
	 * 
	 * @param clazz
	 *            type to empty
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 */
	public void truncate(String clazz) throws TypeNotFoundException;

}
