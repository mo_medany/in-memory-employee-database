package com.mondiamedia.memory.db;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mondiamedia.memory.db.exceptions.IDAlreadyExistsException;
import com.mondiamedia.memory.db.exceptions.IDNotFoundException;
import com.mondiamedia.memory.db.exceptions.TypeAlreadyFoundException;
import com.mondiamedia.memory.db.exceptions.TypeNotFoundException;

/**
 * In-Memory based implementation of {@code DDLActions}, {@code DMLActions}
 * interfaces. This implementation provides basic database operations for this
 * store.
 * 
 * @author medany
 */

public final class InMemoryDataStore implements DDLActions, DMLActions {

	private static final Logger logger = LoggerFactory.getLogger(InMemoryDataStore.class);

	/**
	 * {@code InMemoryDataStore} instance
	 */
	private static InMemoryDataStore INSTANCE;

	/**
	 * default objects store
	 */
	private static Map<String, Map<Object, Object>> STORE;

	/**
	 * don't let anyone instantiate this class.
	 */
	private InMemoryDataStore() {
	}

	/**
	 * Initializes {@code InMemoryDataStore} instance when method is called for the
	 * first time. This technique ensures that {@code InMemoryDataStore} instance is
	 * created only when needed.
	 */
	public static InMemoryDataStore getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new InMemoryDataStore();
			STORE = new HashMap<>();
		}
		logger.info("InMemoryDataStore instance created");
		return INSTANCE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void create(String clazz) throws TypeAlreadyFoundException {
		if (STORE.containsKey(clazz)) {
			logger.error("Error create <" + clazz + ">" + " type");
			throw new TypeAlreadyFoundException("Error create '" + clazz + "'. type already exists");
		}
		STORE.put(clazz, new HashMap<Object, Object>());
		logger.info("<" + clazz + ">" + " type created");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void drop(String clazz) throws TypeNotFoundException {
		if (!STORE.containsKey(clazz)) {
			logger.error("Error drop <" + clazz + ">" + " type");
			throw new TypeNotFoundException("Error drop '" + clazz + "'. type not fount");
		}
		STORE.remove(clazz);
		logger.info("<" + clazz + ">" + " type dropped");
	}

	@Override
	public void truncate(String clazz) throws TypeNotFoundException {
		if (!STORE.containsKey(clazz)) {
			logger.error("Error truncate <" + clazz + ">" + " type");
			throw new TypeNotFoundException("Error truncate '" + clazz + "'. type not fount");
		}
		STORE.get(clazz).clear();
		logger.info("<" + clazz + ">" + " type truncated");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object insert(Object id, Object entry) throws TypeNotFoundException, IDAlreadyExistsException {
		if (!STORE.containsKey(entry.getClass().getName())) {
			logger.error("Error insert <" + entry.getClass().getName() + "> : '" + id + "'");
			throw new TypeNotFoundException("Error insert '" + entry.getClass().getName() + "'. type not fount");
		}

		if (STORE.get(entry.getClass().getName()).containsKey(id)) {
			logger.error("Error insert <" + entry.getClass().getName() + "> : '" + id + "'");
			throw new IDAlreadyExistsException("Error insert '" + id + "'. Already exists");
		}

		Object added = STORE.get(entry.getClass().getName()).put(id, entry);
		logger.info("<" + entry.getClass().getName() + "> : '" + id + "' inserted");
		return added;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean delete(Object id, String clazz) throws TypeNotFoundException, IDNotFoundException {
		if (!STORE.containsKey(clazz)) {
			logger.error("Error delete <" + clazz + "> : '" + id + "'");
			throw new TypeNotFoundException("Error drop '" + clazz + "'. type not fount");
		}

		if (!STORE.get(clazz).containsKey(id)) {
			logger.error("Error delete <" + clazz + "> : '" + id + "'");
			throw new IDNotFoundException("Error delete '" + id + "'. does not exists");
		}

		STORE.get(clazz).remove(id);
		logger.info("<" + clazz + "> : '" + id + "' deleted");
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object update(Object id, Object entry) throws TypeNotFoundException, IDNotFoundException {
		if (!STORE.containsKey(entry.getClass().getName())) {
			logger.error("Error update <" + entry.getClass().getName() + "> : '" + id + "'");
			throw new TypeNotFoundException("Error update '" + entry.getClass().getName() + "'. type not fount");
		}

		if (!STORE.get(entry.getClass().getName()).containsKey(id)) {
			logger.error("Error update <" + entry.getClass().getName() + "> : '" + id + "'");
			throw new IDNotFoundException("Error update '" + id + "'. does not exists");
		}

		Object original = STORE.get(entry.getClass().getName()).get(id);
		entry = copyProperties(entry, original, false);
		entry = STORE.get(entry.getClass().getName()).put(id, entry);
		logger.info("<" + entry.getClass().getName() + "> : '" + id + "' updated");
		return entry;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object find(Object id, String clazz) throws TypeNotFoundException, IDNotFoundException {
		if (!STORE.containsKey(clazz)) {
			logger.error("Error find <" + clazz + "> : '" + id + "'");
			throw new TypeNotFoundException("Error find '" + clazz + "'. type not fount");
		}

		if (!STORE.get(clazz).containsKey(id)) {
			logger.error("Error find <" + clazz + "> : '" + id + "'");
			throw new IDNotFoundException("Error find '" + id + "'. does not exists");
		}

		logger.info("<" + clazz + "> : '" + id + "' found");
		return STORE.get(clazz).get(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> Collection<T> findAll(String clazz) throws TypeNotFoundException {
		if (!STORE.containsKey(clazz)) {
			logger.error("Error list all <" + clazz + ">");
			throw new TypeNotFoundException("Error find '" + clazz + "'. type not fount");
		}

		logger.info("total '" + STORE.get(clazz).values().size() + "' : " + clazz + " found");
		return (Collection<T>) STORE.get(clazz).values();
	}

	/**
	 * Returns all values of specified type with specified order.
	 * {@code TypeNotFoundException} is thrown when specified type not present.
	 * 
	 * @param clazz
	 *            type to find all its values
	 * @param order
	 *            desired values order
	 * @return all values of specified type with specified order
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 */
	public <T> List<T> findAllSorted(String clazz, Comparator<? super T> order) throws TypeNotFoundException {
		List<T> objects = new ArrayList<>();
		objects.addAll(findAll(clazz));
		Collections.sort(objects, order);
		return objects;
	}

	/**
	 * Counts all present values of specified type. {@code TypeNotFoundException} is
	 * thrown when specified type not present.
	 * 
	 * @param clazz
	 *            type to count all its values
	 * @return total count of specified type values, or 0 when specified type is
	 *         empty
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 */
	public int count(String clazz) throws TypeNotFoundException {
		if (!STORE.containsKey(clazz)) {
			logger.error("Error count <" + clazz + ">");
			throw new TypeNotFoundException("Error count '" + clazz + "'. type not fount");
		}

		if (!STORE.get(clazz).equals(null)) {
			logger.error("total '" + STORE.get(clazz).values().size() + "' : " + clazz + " found");
			return STORE.get(clazz).size();
		}

		logger.info("total '0' : " + clazz + " found");
		return 0;
	}

	/**
	 * Copy property values from source to target where the property names are the
	 * same. With ability to allow|deny copy of null values.
	 * 
	 * @param source
	 *            object to copy field values from
	 * @param target
	 *            object to copy field values to
	 * @param copyNull
	 *            allow copy null values
	 * @return copied target object
	 */
	private Object copyProperties(Object source, Object target, boolean copyNull) {
		Field[] fields = source.getClass().getDeclaredFields();
		for (Field f : fields) {
			f.setAccessible(true);
			try {
				Object value = f.get(source);
				if (value != null && !Modifier.isFinal(f.getModifiers()))
					f.set(target, value);
			} catch (ReflectiveOperationException e) {
				logger.error("Error copy " + f, e);
				throw new RuntimeException(e);
			}
		}
		return target;
	}
}
