package com.mondiamedia.memory.db;

import java.util.Collection;

import com.mondiamedia.memory.db.exceptions.IDAlreadyExistsException;
import com.mondiamedia.memory.db.exceptions.IDNotFoundException;
import com.mondiamedia.memory.db.exceptions.TypeNotFoundException;

/**
 * {@code DMLActions} represents basic {@code SELECT}, {@code UPDATE},
 * {@code INSERT} and {@code DELETE} DML commands.
 * 
 * @author medany
 */

public interface DMLActions {

	/**
	 * Associates specified identifier with specified value in this store.
	 * {@code TypeNotFoundException} is thrown when specified type not present,
	 * {@code IDAlreadyExistsException} is thrown when specified identifier already
	 * exists.
	 * 
	 * @param id
	 *            identifier of the value to associate
	 * @param clazz
	 *            value to associate
	 * @return inserted value
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 * @throws IDAlreadyExistsException
	 *             if specified id already exists
	 */
	public Object insert(Object id, Object clazz) throws TypeNotFoundException, IDAlreadyExistsException;

	/**
	 * Removes specified identifier from this store if present.
	 * {@code TypeNotFoundException} is thrown when specified type not present,
	 * {@code IDNotFoundException} is thrown when specified identifier not present.
	 * 
	 * @param id
	 *            identifier of the value to delete
	 * @param clazz
	 *            type of identifier to delete
	 * @return true if specified identifier and value is deleted
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 * @throws IDNotFoundException
	 *             if specified id not present
	 */
	public boolean delete(Object id, String clazz) throws TypeNotFoundException, IDNotFoundException;

	/**
	 * Updates value associated by specified identifier in this store with specified
	 * value. {@code TypeNotFoundException} is thrown when specified type not
	 * present, {@code IDAlreadyExistsException} is thrown when specified identifier
	 * does not exist.
	 * 
	 * @param id
	 *            identifier of the value to update
	 * @param clazz
	 *            value to update specified identifier associated value
	 * @return updated value
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 * @throws IDNotFoundException
	 *             if this id not present
	 */
	public Object update(Object id, Object clazz) throws TypeNotFoundException, IDNotFoundException;

	/**
	 * Returns the value to which specified identifier is associated.
	 * {@code TypeNotFoundException} is thrown when specified type not present,
	 * {@code IDNotFoundException} is thrown when specified identifier not present.
	 * 
	 * @param id
	 *            identifier of value to find
	 * @param clazz
	 *            type of identifier to find
	 * @return value with the specified identifier
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 * @throws IDNotFoundException
	 *             if this identifier not present in database
	 */
	public Object find(Object id, String clazz) throws TypeNotFoundException, IDNotFoundException;

	/**
	 * Returns all values of specified type. {@code TypeNotFoundException} is thrown
	 * when specified type not present.
	 * 
	 * @param clazz
	 *            type to find all its values
	 * @return all values of specified type
	 * @throws TypeNotFoundException
	 *             if specified type not present
	 */
	public <T> Collection<T> findAll(String clazz) throws TypeNotFoundException;
}
