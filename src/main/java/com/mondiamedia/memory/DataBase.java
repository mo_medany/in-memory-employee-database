package com.mondiamedia.memory;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mondiamedia.memory.db.AppConstants;
import com.mondiamedia.memory.db.InMemoryDataStore;
import com.mondiamedia.memory.entities.Employee;

/**
 * @author medany
 */

public class DataBase {

	private static final Logger logger = LoggerFactory.getLogger(DataBase.class);

	private static Scanner SC = new Scanner(System.in);
	private static EmployeeStore Employees = new EmployeeStore(InMemoryDataStore.getInstance());

	// tolerate all unicode hyphens
	private static final String HYPHENS = "[\u002D\u058A\u05BE\u1400\u1806\u2010-\u2015\u2053\u207B\u208B\u2212\u2E17\u2E1A\u2E3A-\u301C\u3030\u30A0\uFE31\uFE32\uFE58\uFE63\uFF0D]";
	private static final String CHARACTERS_REGEX = "[a-zA-Z ]+";
	private static final String DECIMAL_REGEX = "[0-9]+";
	private static final String FRACTION_REGEX = "([0-9]*)\\.([0-9]*)";
	private static final String UPDATE_ACTION_TEXT_REGEX = "(?i)" + AppConstants.UPDATE_ACTION_NAME + "|"
			+ AppConstants.UPDATE_ACTION_DESIG;
	private static final String UPDATE_ACTION_NUMBER_REGEX = "(?i)" + AppConstants.UPDATE_ACTION_SALARY;
	private static final String SORT_REGEX = "(?i)" + AppConstants.SORT_ASCENDING + "|" + AppConstants.SORT_DESCENDING;

	public static void main(String[] args) {

		logger.info("Starting database app");

		welcome();

		String line, output;
		while (true) {
			line = SC.nextLine();
			if (!line.trim().isEmpty()) {
				output = runCommandWithParams(line);
				if (!output.isEmpty())
					System.out.println("\n" + output + "\n");
			}
		}
	}

	/**
	 * Print welcome message to stdout
	 */
	private static void welcome() {
		logger.info("Display welcome message");
		System.out.println("Welcome to Database App");
	}

	/**
	 * Quit application with sepcified exit code
	 * 
	 * @param code
	 */
	private static void quit(int code) {
		SC.close();
		System.out.print("The application will exit.");
		logger.info("Terminating database app");
		System.exit(code);
	}

	/**
	 * Run specified command and parameters from stdin. It begin by parsing the
	 * input to identify the command and validate command parameters count and type.
	 * 
	 * @param input
	 *            input from stdin
	 * @return success or failure message
	 */
	public static String runCommandWithParams(String input) {

		Object[] output = parse(input);
		String command = output[0].toString();
		Object params = output[1];

		if (params != null)
			switch (command.toLowerCase()) {
			case AppConstants.COMMAND_ADD:
				if (params instanceof Employee)
					return Employees.addEmployee((Employee) params);
				break;

			case AppConstants.COMMAND_DELETE:
				if (params instanceof Integer)
					return Employees.deleteEmployee((Integer) params);
				break;

			case AppConstants.COMMAND_UPDATE:
				if (params instanceof Employee)
					return Employees.updateEmployee((Employee) params);
				break;

			case AppConstants.COMMAND_PRINT:
				if (params instanceof Integer)
					return Employees.findEmployee((Integer) params);
				break;

			case AppConstants.COMMAND_PRINT_ALL:
				if (params instanceof String && ((String) params).matches(SORT_REGEX))
					return Employees.findAllSorted((String) params);
				break;

			case AppConstants.COMMAND_QUIT:
				quit(0);

			case AppConstants.COMMAND_EMPTY:
				return Employees.truncate();

			default:
				return "Invalid command '" + command + "'";
			}
		return params.toString();

	}

	/**
	 * Parse the input from stdin to identify the command and validate command
	 * parameters count and type.
	 * 
	 * @param input
	 *            input from stdin
	 * @return object array contains parsed command and validated parameters or
	 *         failure message
	 */
	private static Object[] parse(String input) {

		String[] commandWithParams = input.split(" ", 2);
		String command = input.split(" ", 2)[0], params = "";

		params = commandWithParams.length == 2 ? input.split(" ", 2)[1] : "";

		String[] args;
		int id = 0;
		double salary = 0d;
		switch (command.toLowerCase()) {

		case AppConstants.COMMAND_ADD:
			args = params.split(HYPHENS);
			if (args.length != 4 || !args[0].matches(DECIMAL_REGEX) || !args[1].matches(CHARACTERS_REGEX)
					|| !args[2].matches(CHARACTERS_REGEX)
					|| (!args[3].matches(DECIMAL_REGEX) && !args[3].matches(FRACTION_REGEX))) {
				return new Object[] { command, "add <employee id>-<employee name>-<designation>-<monthly salary>" };
			}
			id = Integer.parseInt(args[0]);
			salary = Double.parseDouble(args[3]);
			return new Object[] { command, new Employee(id, args[1], args[2], salary) };

		case AppConstants.COMMAND_DELETE:
			args = params.split(HYPHENS);
			if (args.length != 1 || !args[0].matches(DECIMAL_REGEX))
				return new Object[] { command, "del <employee id>" };
			id = Integer.parseInt(args[0]);
			return new Object[] { command, id };

		case AppConstants.COMMAND_UPDATE:
			args = params.split(HYPHENS);
			if (args.length != 3 || !args[0].matches(DECIMAL_REGEX)
					|| (!args[1].matches(UPDATE_ACTION_TEXT_REGEX) && (!args[1].matches(UPDATE_ACTION_NUMBER_REGEX)))
					|| (args[1].matches(UPDATE_ACTION_TEXT_REGEX) && !args[2].matches(CHARACTERS_REGEX))
					|| (args[1].matches(UPDATE_ACTION_NUMBER_REGEX) && !args[2].matches(DECIMAL_REGEX)
							&& !args[2].matches(FRACTION_REGEX))) {
				return new Object[] { command, "update <employee id>-<NAME/DESIG/SALARY>-<New Value>" };
			}
			id = Integer.parseInt(args[0]);
			if (args[1].equalsIgnoreCase(AppConstants.UPDATE_ACTION_NAME)) {
				return new Object[] { command, new Employee(id, args[2], null, null) };
			} else if (args[1].equalsIgnoreCase(AppConstants.UPDATE_ACTION_DESIG)) {
				return new Object[] { command, new Employee(id, null, args[2], null) };
			} else if (args[1].equalsIgnoreCase(AppConstants.UPDATE_ACTION_SALARY)) {
				salary = Double.parseDouble(args[2]);
				return new Object[] { command, new Employee(id, null, null, salary) };
			}

		case AppConstants.COMMAND_PRINT:
			args = params.split(HYPHENS);
			if (args.length != 1 || !args[0].matches(DECIMAL_REGEX)) {
				return new Object[] { command, "print <employee id>" };
			}
			id = Integer.parseInt(args[0]);
			return new Object[] { command, id };

		case AppConstants.COMMAND_PRINT_ALL:
			args = params.split(HYPHENS);
			if (args.length != 1 || !args[0].matches(SORT_REGEX)) {
				return new Object[] { command, "printall <ASC/DESC>" };
			}
			return new Object[] { command, args[0] };

		case AppConstants.COMMAND_QUIT:
		case AppConstants.COMMAND_EMPTY:
		default:
			return new Object[] { command, "" };
		}
	}
}
