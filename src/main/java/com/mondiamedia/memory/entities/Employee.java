package com.mondiamedia.memory.entities;

import java.io.Serializable;

/**
 * @author medany
 */

public class Employee implements Serializable {

	private static final long serialVersionUID = 4120328104396105930L;

	private int id;
	private String name;
	private String designation;
	private Double salary;

	public Employee(int id, String name, String designation, Double salary) {
		this.id = id;
		this.name = name;
		this.designation = designation;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return getName() + ", Designation " + getDesignation() + ", Salary: " + getSalary().intValue();

	}
}
