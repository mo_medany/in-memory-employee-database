package com.mondiamedia.memory;

import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mondiamedia.memory.db.AppConstants;
import com.mondiamedia.memory.db.InMemoryDataStore;
import com.mondiamedia.memory.db.exceptions.IDAlreadyExistsException;
import com.mondiamedia.memory.db.exceptions.IDNotFoundException;
import com.mondiamedia.memory.db.exceptions.TypeAlreadyFoundException;
import com.mondiamedia.memory.db.exceptions.TypeNotFoundException;
import com.mondiamedia.memory.entities.Employee;

/**
 * {@code EmployeeStore} wraps an {@code InMemoryDataStore} instance
 * 
 * @author medany
 */
public class EmployeeStore {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeStore.class);

	/**
	 * {@code Employee} ascending comparator
	 */
	private final Comparator<Employee> ascComparator = new Comparator<Employee>() {
		@Override
		public int compare(Employee emp1, Employee emp2) {
			return emp1.getName().compareTo(emp2.getName());
		}
	};

	/**
	 * {@code Employee} descending comparator
	 */
	private final Comparator<Employee> descComparator = new Comparator<Employee>() {
		@Override
		public int compare(Employee emp1, Employee emp2) {
			return emp2.getName().compareTo(emp1.getName());
		}
	};

	/**
	 * {@code Employee} qualified class name
	 */
	private static final String EMPLOYEE_CLAZZ = Employee.class.getName();

	/**
	 * {@code InMemoryDataStore} instance
	 */
	private final InMemoryDataStore STORE;

	/**
	 * Constructs {@code EmployeeStore} with specified store and creates
	 * {@code Employee} in it.
	 * 
	 * @param store
	 *            {@code InMemoryDataStore} instance
	 */
	public EmployeeStore(InMemoryDataStore store) {
		STORE = store;
		if (this.STORE != null)
			try {
				STORE.create(EMPLOYEE_CLAZZ);
				logger.info("employee type created");
			} catch (TypeAlreadyFoundException e) {
				logger.info("creating employee type error. type already found ins this store");
				e.getMessage();
			}
	}

	/**
	 * Adds specified employee to this store.
	 * 
	 * @param emp
	 *            employee to add
	 * @return success or failure message
	 */
	public String addEmployee(Employee emp) {
		try {
			STORE.insert(emp.getId(), emp);
			logger.info("added new employee '" + emp.getId() + "'");
			return "Employee '" + emp.getName() + "' added successfully. Total no of employees = "
					+ STORE.count(EMPLOYEE_CLAZZ);
		} catch (TypeNotFoundException e) {
			logger.error("adding new employee error. type not fount in this store", e);
			return e.getMessage();
		} catch (IDAlreadyExistsException e) {
			try {
				logger.error("adding new employee '" + emp.getId() + "'. Already exists", e);
				return "Employee '" + emp.getId() + "' already exists. Total no of employees = "
						+ STORE.count(EMPLOYEE_CLAZZ);
			} catch (TypeNotFoundException e1) {
				logger.info("", e1);
				return e1.getMessage();
			}
		}
	}

	/**
	 * Deletes employee associated to specified identifier in this store.
	 * 
	 * @param id
	 *            identifier of employee to delete
	 * @return success or failure message
	 */
	public String deleteEmployee(Integer id) {
		try {
			STORE.delete(id, EMPLOYEE_CLAZZ);
			logger.info("employee '" + id + "' deleted.");
			return "Employee '" + id + "' deleted successfully. Total no of employees = " + STORE.count(EMPLOYEE_CLAZZ);
		} catch (TypeNotFoundException e) {
			logger.error("delete employee error. type not fount in this store", e);
			return e.getMessage();
		} catch (IDNotFoundException e) {
			logger.error("delete employee error. '" + id + "' not present in this store", e);
			return "Employee '" + id + "' not found";
		}
	}

	/**
	 * Updates specified employee in this store.
	 * 
	 * @param emp
	 *            employee to update
	 * @return success or failure message
	 */
	public String updateEmployee(Employee emp) {
		try {
			emp = (Employee) STORE.update(emp.getId(), emp);
			logger.info("employee '" + emp.getId() + "' updated");
			return "Employee '" + emp.getId() + "' updated. Name: " + emp.getName() + ", Designation: "
					+ emp.getDesignation() + ", Salary: " + emp.getSalary().intValue();
		} catch (TypeNotFoundException e) {
			logger.error("update employee error. type not fount in this store", e);
			return e.getMessage();
		} catch (IDNotFoundException e) {
			logger.error("update employee error. '" + emp.getId() + "' not present in this store", e);
			return "Employee '" + emp.getId() + "' not found";
		}
	}

	/**
	 * Finds employee associated to specified identifier in this store.
	 * 
	 * @param emp
	 *            identifier of employee to find
	 * @return success or failure message
	 */
	public String findEmployee(Integer id) {
		try {
			Employee emp = (Employee) STORE.find(id, EMPLOYEE_CLAZZ);
			logger.info("employee '" + emp.getId() + "' found");
			return emp.toString();
		} catch (TypeNotFoundException e) {
			logger.error("find employee error. type not fount in this store", e);
			return e.getMessage();
		} catch (IDNotFoundException e) {
			logger.error("find employee error. '" + id + "' not present in this store", e);
			return "Employee '" + id + "'  not found";
		}
	}

	/**
	 * Finds all employees with specified order.
	 * 
	 * @param order
	 *            order to return employees with
	 * @return success or failure message
	 */
	@SuppressWarnings("unchecked")
	public String findAllSorted(String order) {
		try {
			List<Employee> employees;

			if (order.equalsIgnoreCase(AppConstants.SORT_DESCENDING))
				employees = (List<Employee>) (Object) STORE.findAllSorted(EMPLOYEE_CLAZZ, descComparator);
			else
				employees = (List<Employee>) (Object) STORE.findAllSorted(EMPLOYEE_CLAZZ, ascComparator);

			logger.info("total '" + employees.size() + "' found");

			StringBuilder output = new StringBuilder();
			for (int i = 0; i < employees.size(); i++) {
				output.append(employees.get(i).getName() + " : " + employees.get(i).getDesignation());
				if (i + 1 != employees.size())
					output.append("\n");
			}
			return output.toString();
		} catch (TypeNotFoundException e) {
			logger.error("find employee error. type not fount in this store", e);
			return e.getMessage();
		}
	}

	/**
	 * Empty employees store.
	 * 
	 * @return success or failure message
	 */
	public String truncate() {
		try {
			STORE.truncate(EMPLOYEE_CLAZZ);
			logger.info("employee store emptied");
			return EMPLOYEE_CLAZZ + " truncated";
		} catch (TypeNotFoundException e) {
			return e.getMessage();
		}
	}
}
